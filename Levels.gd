extends Node

const MAIN = "res://ui/menus/main_menu/MainMenu.tscn"
const GAMEOVER = "res://ui/menus/game_over/GameOver.tscn"
const WIN = "res://ui/menus/win/Win.tscn"
const LEVEL1 = "res://levels/game/Game.tscn"
