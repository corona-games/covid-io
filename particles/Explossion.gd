extends Node2D

onready var emitter = $Particles2D
onready var animation = $AnimationPlayer

func _ready():
	emitter.set_emitting(true)
	animation.play("explode")

func _on_AnimationPlayer_animation_finished(_anim_name):
	queue_free()
