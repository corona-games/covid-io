extends Node

var current:int setget current_set, current_get
var best:int setget ,best_get

func _ready():
	current = 0
	best = 10

func current_set(new_value):
	current = new_value
	if current > best:
		best = current

func current_get():
	return current 
	
func best_get():
	return best 
