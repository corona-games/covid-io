extends Control

onready var score_label = $MarginContainer/VBoxContainer/CenterContainer/VBoxContainer/HBoxContainer/ScoreLabel
onready var best_label = $MarginContainer/VBoxContainer/CenterContainer/VBoxContainer/HBoxContainer2/BestLabel
onready var retry_button = $MarginContainer/VBoxContainer/CenterContainer2/HBoxContainer/PlayAgain

func _ready():
	score_label.text = str(Score.current)
	best_label.text = str(Score.best)
	retry_button.grab_focus()


func _on_PlayAgain_button_up():
	var error = get_tree().change_scene(Levels.MAIN)
	if error != OK:
		print(error)
