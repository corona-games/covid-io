extends Control

onready var play_button = $MarginContainer/MainMenuLayout/CenterContainer/VBoxContainer/Play
onready var best_label = $MarginContainer/MainMenuLayout/HBoxContainer/MaxScore

func _ready():
	play_button.grab_focus()
	best_label.text = str(Score.best)


func _on_Play_button_up():
	var error = get_tree().change_scene(Levels.LEVEL1)
	if error != OK:
		print(error)

func _on_Quit_button_up():
	get_tree().quit()
