extends Control

onready var retry_button = $MarginContainer/VBoxContainer/CenterContainer/HBoxContainer/PlayAgain

func _ready():
	retry_button.grab_focus()


func _on_PlayAgain_button_up():
	var error = get_tree().change_scene(Levels.LEVEL1)
	if error != OK:
		print(error)

func _on_Exit_button_up():
	var error = get_tree().change_scene(Levels.MAIN)
	if error != OK:
		print(error)
