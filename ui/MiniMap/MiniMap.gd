extends MarginContainer

onready var player = get_node("../../Player")

export var zoom = 1.5

onready var grid = $Grid
onready var player_marker = $Grid/Player
onready var enemy_marker = $Grid/Enemy

onready var particle_marker = $Grid/Particle
onready var changer_marker = $Grid/ParticleChanger
onready var resizer_marker = $Grid/ParticleResizer
onready var speeder_marker = $Grid/ParticleSpeeder

var grid_scale
var markers = {}

func _ready():
	player_marker.position = grid.rect_size / 2
	grid_scale = grid.rect_size / (get_viewport_rect().size * zoom)
	var map_objects = get_tree().get_nodes_in_group("mini_map_objects")
	for mini_map_object in map_objects:
		var new_marker = particle_marker.duplicate()
		
		if mini_map_object.is_in_group("enemies"):
			new_marker = enemy_marker.duplicate() 
		
		if mini_map_object.is_in_group("resizers"):
			new_marker = resizer_marker.duplicate()
		
		if mini_map_object.is_in_group("movchangers"):
			new_marker = changer_marker.duplicate()
		
		if mini_map_object.is_in_group("speeders"):
			new_marker = speeder_marker.duplicate()
		
		grid.add_child(new_marker)
		new_marker.show()
		markers[new_marker] = mini_map_object


func _process(_delta):
	if !player:
		return
	for marker in markers:
		var mini_map_object = markers[marker]
		if is_instance_valid(mini_map_object):
			var obj_pos = (mini_map_object.position - player.position) * grid_scale + grid.rect_size / 2
			
			if !grid.get_rect().has_point(obj_pos + grid.rect_position):
				marker.scale = Vector2(.3,.3)
			else:
				marker.scale = Vector2(.5,.5)
			
			obj_pos.x = clamp(obj_pos.x, 0, grid.rect_size.x)
			obj_pos.y = clamp(obj_pos.y, 0, grid.rect_size.y)
			marker.position = obj_pos
		else:
			markers.erase(marker)
			marker.queue_free()
