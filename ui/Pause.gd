extends Control

signal pause_pressed
signal continue_pressed
signal restart_pressed
signal exit_pressed

onready var focus_button = $VBoxContainer/CenterContainer/VBoxContainer/Continue

func _unhandled_input(event):
	if event.is_action_released("pause"):
		if get_tree().is_paused() == false:
			emit_signal("pause_pressed")
			focus_button.grab_focus()
		else:
			emit_signal("continue_pressed")

func _on_Continue_button_up():
	print("continue_pressed")
	emit_signal("continue_pressed")

func _on_Restart_button_up():
	print("restart_pressed")
	emit_signal("restart_pressed")

func _on_Exit_button_up():
	print("exit_pressed")
	emit_signal("exit_pressed")
