extends Button

onready var select_player = $Select
onready var press_player = $Press
onready var anim_player = $AnimationPlayer

func _ready():
	pass


func _on_MyButton_pressed():
	press_player.play()


func _on_MyButton_focus_exited():
	select_player.play()
	anim_player.stop()


func _on_MyButton_focus_entered():
	anim_player.play("selected")
