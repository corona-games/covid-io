extends Control

onready var label = $MarginContainer/Label
onready var player = $AnimationPlayer

func display(mytext):
	label.percent_visible = 0
	label.bbcode_text = mytext
	player.play("show_message")

func _ready():
	label.percent_visible = 0
	visible = false
