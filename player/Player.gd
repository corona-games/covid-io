extends Area2D

export var speed = 200
export var speed_factor = 1.5
export var scale_factor = 1.2

signal particle_collected
signal enemy_collided
signal camera_exited

var explossion = preload("res://particles/Explossion.tscn")
onready var _crazy_voice = $Voice/Crazy
onready var _yummy_voice = $Voice/Yummy


func _ready():
	pass 


func _process(delta):
	var velocity = Vector2() 
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		
	position += velocity * delta


func _on_Player_area_entered(area):
	if area.is_in_group("particles"):
		print ("particle collected")
		emit_signal("particle_collected")
		
		var expl = explossion.instance()
		
		expl.position = area.position
		get_parent().add_child(expl)
		
		area.queue_free()
		
	if area.is_in_group("speeders"):
		print ("speed up")
		speed *= speed_factor
	
	if area.is_in_group("resizers"):
		print ("scale up")
		scale *= scale_factor		
		_yummy_voice.play()
	
	if area.is_in_group("movchangers"):
		print ("controls changed")
		speed = -speed
		_crazy_voice.play()
	
	if area.is_in_group("enemies"):
		print ("enemy collided")
		var expl = explossion.instance()
		expl.position = position
		get_parent().add_child(expl)
		#set_process(false)
		self.hide()
		emit_signal("enemy_collided")


func _on_VisibilityNotifier2D_screen_exited():
	emit_signal("camera_exited")
