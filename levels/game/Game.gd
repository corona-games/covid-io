extends Node2D

onready var _counter = $UI/Markers/MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/Counter
onready var _time_label = $UI/Markers/MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer2/TimeLabel

onready var _pause_menu = $UI/Pause
onready var _player = $Player

onready var _death_sound_player = $Sound/Death
onready var _collect_sound_player = $Sound/Collect
onready var _win_sound_player = $Sound/Win

onready var _message_box = $UI/MessageBox

onready var _hurry_up_voice = $Sound/hurry

var points = 0
export var time = 30
export var next_scene_path = Levels.WIN

var _playing = false

func _ready():
	points = 0
	_playing = false
	_counter.text = str(points)
	
	_player.connect("particle_collected", self , "_particle_collected")
	_player.connect("enemy_collided", self , "_game_over")
	_player.connect("camera_exited", self, "_player_outbounds")

	_pause_menu.connect("pause_pressed", self, "_pause")
	_pause_menu.connect("continue_pressed", self, "_continue")
	_pause_menu.connect("exit_pressed", self, "_exit")
	_pause_menu.connect("restart_pressed", self, "_restart")
	
	_playing = true
	
	_message_box.display("Infection Started !!! ")

func _game_over():
	print("GAME OVER")
	_death_sound_player.play()
	_player.set_process(false)
	_playing = false
	_message_box.display("Desinfected ...")
	yield(get_tree().create_timer(3.0), "timeout")
	var error = get_tree().change_scene(Levels.GAMEOVER)
	if error != OK:
		#print("Game Over Change Scene Failure!: " + error)
		print(error)

func _game_win():
	print("YOU WIN")
	points += time
	Score.current = points
	_win_sound_player.play()
	_player.set_process(false)
	_playing = false
	_message_box.display("Infection Completed !!! ")
	yield(get_tree().create_timer(1.0), "timeout")
	var error = get_tree().change_scene(next_scene_path)
	if error != OK:
		  print("Game WIN Change Scene Failure!: " + error)

func _particle_collected():
	points += 10
	_counter.text = str(points)
	_collect_sound_player.play()
	var particles = $Particles.get_child_count()
	if particles < 2:
		_game_win()

func _on_Timer_timeout():
	if _playing:
		time -= 1
		_time_label.text = str(time)
		if time == 10:
			_message_box.display("Hurry Up !!! ")
			_hurry_up_voice.play()
		if time <= 0:
			_game_over()

func _pause():
	_pause_menu.show()
	_playing = false
	get_tree().paused = true

func _continue():
	_pause_menu.hide()
	_playing = true
	get_tree().paused = false

func _restart():
	_playing = false
	var error = get_tree().reload_current_scene()
	if error != OK:
		  print(error)
	get_tree().paused = false

func _exit():
	_playing = false
	var error = get_tree().change_scene(Levels.MAIN)
	if error != OK:
		  print(error)
	get_tree().paused = false

func _player_outbounds():
	if _playing:
		print("Player out")
		_game_over()
