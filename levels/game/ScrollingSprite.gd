extends Sprite

onready var _ypos = 0
onready var _xpos = 0

export var y_speed = 8
export var x_speed = 16


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_move_region(delta)

func _move_region(delta):
	# Si esta froma de scroll no consigue el rendimiento adecuado,
	# estudiar alternativa con shader
	_ypos -= y_speed * delta
	_xpos -= x_speed * delta
	var dimx = texture.get_width()
	var dimy = texture.get_height()

	set_region_rect(Rect2(_xpos,_ypos,dimx,dimy))
